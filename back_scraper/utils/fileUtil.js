'use strict'

var fs = require('fs');

function saveJsonFile(json, name) {

    fs.writeFile(name+'.json', JSON.stringify(json, null, 4), function(err){
        if(err) {
            return false;
        } else {
            console.log('el archivo se escribio correctamente.' + name);
        }
    });

    return true;
}

module.exports = {
    saveJsonFile
}