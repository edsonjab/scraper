'use strict'

// Librerias
var express = require('express');
var bodyParser = require('body-parser');
var app     = express();

//Variables
var host = 'localhost';
var port = 3000;

// Rutas
var scraper_route = require('./routes/scrape_route');

// Configuracion
app.use(bodyParser.json({limit: '5mb'}));
app.use(bodyParser.urlencoded({limit: '5mb', extended: true}));

// Configurar CORS
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, PUT, DELETE, POST");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

// Carga de rutas
app.use('/api', scraper_route);

// Ejecuta servidor
app.listen(port, () => {
    console.log('El servidor esta corriendo en ' + host + ':' + port);
});