'use strict'

var request = require('request');
var cheerio = require('cheerio');
var fileUtils = require('../utils/fileUtil');

function scrape(req, res) {
    console.log(req.body)
    var url = req.body.pagina;
    var pais = req.body.pais;

    request(url, function(error, response, html) {

        if(!error) {
            var $ = cheerio.load(html);

            var nombrePagina = new Array(), totalFans = new Array(), json = new Array();

            // Obtiene nombres de la página
            $('.show-name').each(function(i, elem) {
                nombrePagina.push(elem.firstChild.data.trim());
            });

            //Obtiene total de fans
            $('.brand-table-list > tbody > tr > td > .item').each(function(i, elem) {
                $(this).find('strong').each(function(i, elem) {
                    let result = $(this).text().trim();
                    if(result) {
                        totalFans.push(result);
                    }
                });
            });

            // Forma JSON 
            for(var i = 0; i < nombrePagina.length; i++) {
                json.push({
                    "Página" : nombrePagina[i],
                    "Total Fans" : totalFans[i]
                });
            }

            if(fileUtils.saveJsonFile(json, pais)) {
                res.status(200).json({
                    mensaje: 'Se genero el archivo correctamente'
                });
            } else {
                res.status(500).json({
                    mensaje: 'Ocurrio un error al generar el archivo'
                });
            }

            

        } else {
            res.status(500).json({ message: "Ocurrio un error en el servidor" + error});
        }
    });

}

module.exports = {
    scrape
}