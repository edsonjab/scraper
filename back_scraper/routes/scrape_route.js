'use strict'

var express = require('express');

var scrapeController = require('../controllers/scrapeCtrl');

var api = express.Router();

api.post('/scrape', scrapeController.scrape);

module.exports = api;