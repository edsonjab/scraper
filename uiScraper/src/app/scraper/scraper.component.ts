import { Component, OnInit } from '@angular/core';
import { Scraper } from '../models/scraper';
import { ScraperService } from '../services/scraper.service';

@Component({
  selector: 'app-scraper',
  templateUrl: './scraper.component.html',
  styleUrls: ['./scraper.component.css'],
  providers: [
    ScraperService
  ]
})
export class ScraperComponent implements OnInit {

  private scraper: Scraper;
  private listaPais: any;

  constructor(
    private _scrapeService: ScraperService
  ) {
    this.scraper = new Scraper();
    this.listaPais = [
      { pais: "México", pagina: "mexico"},
      { pais: "España", pagina: "spain"},
      { pais: "Estados Unidos", pagina: "united-states"}];
  }

  ngOnInit() {

  }

  exportFile(pais: Scraper) {
    this._scrapeService.scrapPage(this.scraper).subscribe(
      data => {
        if(data){
          alert("Creación de archivo lista")
        }else{
          alert("Error en la creación")
        }
      },
      error => {
          console.log(<any>error);
      }
    );
  }

  setPagina(pagina:string) {
    this.scraper.pagina = "https://www.socialbakers.com/statistics/facebook/pages/total/" + pagina;
}
 
}
