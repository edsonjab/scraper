import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Scraper } from '../models/scraper';
import { environment } from '../../environments/environment';

@Injectable()
export class ScraperService {

    private url: string;

    constructor(private http: HttpClient) {
      this.url = `${environment.api}`;
    }

    scrapPage(scraper: Scraper){
        return this.http.post(this.url+'scrape', scraper);
    }

}